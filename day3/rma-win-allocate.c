#include <stdio.h>
#include <stdlib.h>

#include <mpi.h>

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);

  MPI_Comm comm = MPI_COMM_WORLD;

  int size;
  MPI_Comm_size(comm, &size);
  if (size != 2) {
    printf(
        "This application is meant to be run with 2 MPI processes, not %d.\n",
        size);
    MPI_Abort(comm, EXIT_FAILURE);
  }

  // Get my rank
  int rank;
  MPI_Comm_rank(comm, &rank);

  /* FIXME create the window */
  // Create the window
  int *window_buffer;
  MPI_Win win;
  MPI_Win_allocate(
      (MPI_Aint) 4 * sizeof(int)  /* MPI_Aint size */,
       sizeof(int)  /* int disp_unit */,
       MPI_INFO_NULL  /* MPI_Info info */,
       comm  /* MPI_Comm comm */,
       &window_buffer  /* void *baseptr */,
       &win  /* MPI_Win *win */
  );

  // start access epoch
  // NOTE: See if we really need this fence
  // It works without the fence call, but anything which happens to a window
  // buffer should happen in epochs
  MPI_Win_fence(0, win);

  // local store
  if (rank == 1) {
    window_buffer[0] = 42;
    window_buffer[1] = 88;
    window_buffer[2] = 12;
    window_buffer[3] = 3;
  }

  // start access epoch
  MPI_Win_fence(0, win);

  int getbuf[4];
  if (rank == 0) {
    // Fetch the value from the MPI process 1 window
    MPI_Get(&getbuf, 4, MPI_INT, 1, 0, 4, MPI_INT, win);
  }

  // end access epoch
  MPI_Win_fence(0, win);

  if (rank == 0) {
    printf("[MPI process 0] Value fetched from MPI process 1 window:");
    for (int i = 0; i < 4; ++i) {
      printf(" %d", getbuf[i]);
    }
    printf("\n");
  }

  // Destroy the window
  MPI_Win_free(&win);

  MPI_Finalize();

  return EXIT_SUCCESS;
}
