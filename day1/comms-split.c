#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  int size, rank;

  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // create a communicator
  MPI_Comm town;
  // 2nd arg: condition. 3rd arg: key, specifies an ordering
  MPI_Comm_split(MPI_COMM_WORLD, rank % 2, rank, &town);

  int town_size, town_rank;
  MPI_Comm_size(town, &town_size);
  MPI_Comm_rank(town, &town_rank);

  printf(" rank %d town_rank %d; ", rank, town_rank);
  printf(" size %d town_size %d\n", size, town_size);

  MPI_Comm_free(&town);

  MPI_Finalize();

  return 0;
}
