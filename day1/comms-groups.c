#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  int size, rank;

  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  printf(" rank %d in MPI_COMM_WORLD of size %d\n", rank, size);

  // "retreive" a group 1
  MPI_Group world_group;
  MPI_Comm_group(MPI_COMM_WORLD, &world_group);

  // another group 2
  MPI_Group most_popular;

  // Exclude processes from world_group in most_popular
  int ranks[1];
  ranks[0] = 1;
  // NOTE: world_group by value, most_popular by ref
  MPI_Group_excl(world_group, 1, ranks, &most_popular);

  // uses some memory; to free them:
  MPI_Group_free(&world_group);

  // create a communicator
  MPI_Comm town;
  MPI_Comm_create(MPI_COMM_WORLD, most_popular, &town);

  MPI_Finalize();

  return 0;
}
