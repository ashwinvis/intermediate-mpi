#include <stdio.h>

#include <mpi.h>

int main(int argc, char *argv[]){
  MPI_Init(&argc, &argv);

  MPI_Aint lb, extent;
  int size;

  // char
  // NOTE: ld because long integers
  printf("sizeof(char) = %ld\n", sizeof(char));
  MPI_Type_get_extent(MPI_CHAR, &lb, &extent);
  MPI_Type_size(MPI_CHAR, &size);
  printf("For MPI_CHAR: lb= %ld, extent = %ld. size= %d\n", lb, extent, size);

  MPI_Finalize();

  return 0;
}
